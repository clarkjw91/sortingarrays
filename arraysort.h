#ifndef ARRAYSORT_H_
#define ARRAYSORT_H_
//typedef for flexability later
typedef int n_type;
enum sortType {Q_SORT, B_SORT,S_SORT};
class ArraySort
{
public:
  ArraySort(n_type*, int);
  ArraySort(n_type*);
  bool sortManager(sortType = Q_SORT);
  //void printArray();


private:
  n_type * array;
  int array_size;

  bool qsort(int, int = 0); //quick sort
  bool sort(); //sort
  bool bsort(); //bubble sort
  void swap(int, int);//swap helper function
};
#endif // ARRAYSORT_H_


//whats the class will do
//Template allowing numeric values
//ints array qSort, swapsort, bubble sort
//look at overloading for chars
