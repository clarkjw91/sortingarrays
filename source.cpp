#include "arraysort.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <ctime>

int main(){
  int s = 10000;
  int range = 100;
  int offset = 1;
  int nums[s], nums2[s], nums3[s];
  int number;
clock_t now, then;
double elapsed;
  srand(time(NULL));
  //fill array
    for(int i = 0; i < s; i++){
    nums[i] = nums2[i] = nums3[i] = (rand() % range) + offset;
    }
  sortType ch;
  ArraySort intQSorter(nums, s);
  ArraySort intBSorter(nums2, s);
  ArraySort intSSorter(nums3, s);
  std::cout << "\n\nSorting...\n";
  ch = Q_SORT;
  //QSort Time Testing
  then = clock();
  intQSorter.sortManager(ch);
  now = clock();
  elapsed = double(now - then);
  std::cout << "QSort Time:" << elapsed << "ns\n";

  ch = B_SORT;
  //QSort Time Testing
  then = clock();
  intBSorter.sortManager(ch);
  now = clock();
  elapsed = double(now - then);
  std::cout << "BSort Time:" << elapsed << "ns\n";

  ch = S_SORT;
  //QSort Time Testing
  then = clock();
  intSSorter.sortManager(ch);
  now = clock();
  elapsed = double(now - then);
  std::cout << "SSort Time:" << elapsed << "ns\n";



  return 0;
}
