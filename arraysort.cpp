#include <iostream>

#include "arraysort.h"

ArraySort::ArraySort(n_type * a, int size)
{
  array = a;
  array_size = size;
}

void ArraySort::swap(int a, int b)
{
  n_type temp = array[a];
  array[a] = array[b];
  array[b] = temp;
}

bool ArraySort::qsort(int hi, int lo){
  int i, p;
  n_type pElem;
  if(lo < hi){
    //partitioning
    pElem = array[hi]; //set pivot
    i = lo -1; //not sure why
    for(int j = lo; j < hi; j++){//swapping compared to pivot elem
      if(array[j] < pElem){
        i++;
        swap(j, i);
      }
    }
    if(array[hi] < array[i + 1])//if hi < low
        swap((i + 1), hi);
    p = i + 1;//creat hi elem for rec

    qsort((p -1),lo);
    qsort(hi, (p + 1));
  }else{
    return true;
  }
}

bool ArraySort::bsort(){
  bool swapped;
  do{
    swapped = false;
    for(int i = 1; i < array_size; i++){
      if(array[i - 1] > array[i]){
        swap((i - 1), i);
        swapped = true;
      }
    }
  }while(swapped);
  return true;
}

bool ArraySort::sort(){
  int hInd = array_size -1;
  int mInd;
  while(hInd >= 0){
     mInd = hInd;
    for(int i = 0; i < hInd; i++){
      if(array[mInd] < array[i]){
        mInd = i;
      }
    }
   swap(hInd, mInd);
    hInd--;
  }
}

bool ArraySort::sortManager(sortType ch)
{
  switch(ch){
    case Q_SORT: {
    return qsort(array_size - 1);

    }
    case B_SORT: {
    return bsort();
    }
    case S_SORT: {
    return sort();
    }
  }
}

//Print function useful during dev
//to be deleted
/*
void ArraySort::printArray()
{
  for(int i = 0; i < array_size; i++)
  {
  std::cout << "\t ElV: " << array[i];
  }
  std::cout << "\n\n";
}*/
